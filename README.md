## Привет, я Аня Верткова :wave:
***Нахожусь в поиске работы manual QA engineer.***

Сейчас работаю дизайнером, с расширенным функционалом обязанностей, в него входит:
- Написание чек-листов для тестирования сайтов и проведение функционального тестирование корпоративных сайтов;
- Разработка макетов страниц и карты корпоративного сайта, его наполнение и тестирование;
- Составление технического задание на разработку сайта на WordPress;
- Поддержка корпоративного сайта на Joomla - создание, удаление и доработка существующих страниц сайта;
- Создание лендинга - макет (Figma), верстка (Bootstrap);
- Создание и поддержание сайта с помощью WordPress;
- Разработка графических материалов;
- Верстка каталогов, паспортов и технической документации;
- Адаптация и перевод материалов для российского рынка.


Далее ссылки и описание учебных проектов.

## [НЕВА - пожарные гидранты](https://gitlab.com/learning_qa/neva)
Изучая HTML, CSS, Bootstrap и немножечко JS, я сделала лендинг для пожарных гидрантов. 
#### Документация по проекту: ####
- [Карта midmap](https://miro.com/app/board/o9J_lOBPTuk=/?invite_link_id=130381764257/)
- Макет в [Figma](https://www.figma.com/file/EmAVBvp1A8Zf4MDXJoQd4W/%D1%81%D0%B0%D0%B9%D1%82-%D0%9D%D0%95%D0%92%D0%90?node-id=85%3A0) / [PDF](https://drive.google.com/drive/folders/1c4RbEYRc-UPgMLWiebDFmS_ID5DWE1lF?usp=sharing)
- [Спецификация, чек-лист, тест-кейсы](https://docs.google.com/spreadsheets/d/1VAzKH3rWIAcHHA4IKTJMPtLu77gIl37IxdbinnXxCKU/edit?usp=sharing)

---

[*Пример Bug-report для публичных ресурсов*](https://docs.google.com/spreadsheets/d/1Q9iSAoMhztqBXwZmm0D-Im0LwXhp7XkPHvLS0oLU1po/edit?usp=sharing)


## [SQL](https://gitlab.com/learning_qa/mysql)
Прошла курс *Интерактивный тренажер по SQL*. На основе знаний из курса создала каталог библиотеки в MySQL. [Примеры запросов из курса.](https://docs.google.com/document/d/1R-10f-NULnOAIIFLK3x2cKlEXAZQRWn4O-Ui9aVBeJ0/edit?usp=sharing)

## [Курс Ксендзова](https://gitlab.com/learning_qa/ksendzov_qa)
Прошла вводную часть курса Вадима, изучила основные команды для работы с terminal и git.

## Postman
Прошла курс [*Тестирование ПО: Postman для тестирования API*](https://drive.google.com/file/d/1zQj4H2gy7MvNE-0VvRIEby6TySQTpc8c/view?usp=share_link) на stepik.
#### Примеры колекций:
[*Тестовая коллекция на основе Petstore server*](https://elements.getpostman.com/redirect?entityId=21095012-7cbcd389-e5d8-4489-8f0d-bea1472a37ef&entityType=collection) 

В колекции три запроса. Первый POST-запрос создает нового питомца, в тесте прописан скрипт который забирает ID из тела ответа и записывает значение в переменную коллекции. Второй GET-запрос выводит информацию о питомце используя записанный в коллекцию ID, в тесте прописан скрипт который берет тело ответа, изменяет статус на "sold" и перенаправляет измененное тело PUT-запросом. Третий GET-запрос выводит информацию о питомце используя ID из коллекции, в тесте прописан скрипт проверяющий, что статус "sold".

[*Расчет страхового полиса с использованием открытой API AgentApp b2b*](https://www.postman.com/payload-candidate-19415483/workspace/calculation-of-the-insurance-policy/collection/21095012-cd25d6aa-323c-4291-805e-097f34677d5c?action=share&creator=21095012)

Цель: Составить коллекцию и окружение для получения Расчета полиса в тестовой страховой компании и получить премию.

### Links
:e-mail: [anna.vertkova@gmail.com](mailto:anna.vertkova@gmail.com)

:telephone_receiver: [telegram](https://t.me/Anna_vert)

### Skills  
GitLab | Windows | MacOs | MySQL | Postman | Chrome_DevTools | WordPress | HTML | CSS | Bootstrap | Figma | Miro